import java.util.*;

public class Anagram {
    public static void main(String[] args) {
        List<String> words = List.of("cat", "listen", "silent", "kitten", "salient");
        System.out.println(getGroupedAnagrams(words));
    }

    public static int getGroupedAnagrams(List<String> words) {
        Map<String, List<String>> result = new HashMap<>();
        for (String value : words) {
            char[] characters = value.toCharArray();
            Arrays.sort(characters);
            String sortedValue = new String(characters);

            if (result.containsKey(sortedValue)) {
                result.get(sortedValue).add(value);
            } else {
                List<String> list = new ArrayList<>();
                list.add(value);
                result.put(sortedValue, list);
            }
        }
        return result.size();
    }
}
